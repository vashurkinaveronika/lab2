import javax.swing.*;
import java.awt.*;

public class ViewUpdate extends JPanel {
    private JTextField albumId;
    private JTextField albumName;
    private JTextField executorId;
    private JTextField albumGenre;

    private JButton ok;
    private boolean isVisible;
    private JDialog jDialog;

    public ViewUpdate() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(6, 4));
        panel.add(new JLabel("Album id:"));
        panel.add(albumId = new JTextField(""));
        panel.add(new JLabel("Album name:"));
        panel.add(albumName = new JTextField(""));
        panel.add(new JLabel("Executor id:"));
        panel.add(executorId = new JTextField(""));
        panel.add(new JLabel("Album genre:"));
        panel.add(albumGenre = new JTextField(""));
        add(panel, BorderLayout.CENTER);

        ok = new JButton("Ok");
        ok.addActionListener(event -> {
            isVisible = true;
            jDialog.setVisible(false);
        });


        JPanel buttonPanel = new JPanel();
        buttonPanel.add(ok);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    public boolean showDialog(Component parent, String title) {
        isVisible = false;

        Frame owner = null;
        if (parent instanceof Frame)
            owner = (Frame) parent;
        else
            owner = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, parent);


        if (jDialog == null || jDialog.getOwner() != owner) {
            jDialog = new JDialog(owner, true);
            jDialog.add(this);
            jDialog.getRootPane().setDefaultButton(ok);
            jDialog.pack();
        }

        jDialog.setTitle(title);
        jDialog.setVisible(true);
        return isVisible;
    }

    public int getAlbumId() {
        return Integer.valueOf(albumId.getText());
    }

    public String getAlbumName() {
        return albumName.getText();
    }

    public int getExecutorId() {
        return Integer.valueOf(executorId.getText());
    }

    public String getAlbumGenre() {
        return albumGenre.getText();
    }
}

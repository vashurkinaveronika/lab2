import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class View extends JFrame {
    private static Object[] columnsHeader = new String[]{"album_id", "album_name", "executor_id", "album_genre"};
    private static ArrayList<TableAlbum> records = new ArrayList<>();
    private Object[][] array;
    private static DefaultTableModel tableModel = new DefaultTableModel();
    private static JTable table = new JTable(tableModel);
    SQLSubQuery data = new SQLSubQuery();


    private ViewAdd viewAdd = new ViewAdd();
    private ViewDelete viewDelete = new ViewDelete();
    private ViewQuery viewQuery = new ViewQuery();
    private ViewUpdate viewUpdate = new ViewUpdate();

    public View() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        super("Music store");
        //this.setBounds(100, 100, 1233, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        updateTable();
//добавление удаление редактирование запрос

        JButton query = new JButton("Запрос");
        query.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (viewQuery == null) viewQuery = new ViewQuery();

                if (viewQuery.showDialog(View.this, "Special request")) {
                    String executorName = viewQuery.getExecutorName();

                    ArrayList<String> recordsQuery = null;
                    if (!executorName.equals("")) {
                        recordsQuery = data.doQuery(executorName);
                    }
                    tableModel.getDataVector().removeAllElements();
                    tableModel.setColumnIdentifiers(new String[]{"album_name"});
                    array = new Object[recordsQuery.size()][4];
                    for (int i = 0; i < recordsQuery.size(); i++) {
                        array[i][0] = recordsQuery.get(i);
                    }
                    // Наполнение модели данными
                    for (int i = 0; i < array.length; i++) {

                        tableModel.addRow(array[i]);///?????
                    }
                }
            }
        });
        // Создание кнопки добавления строки таблицы
        JButton add = new JButton("Добавить");
        add.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (viewAdd == null) viewAdd = new ViewAdd();
                if (viewAdd.showDialog(View.this, "Insert")) {
                    Integer albumId = viewAdd.getAlbumId();
                    String albumName = viewAdd.getAlbumName();
                    Integer executorId = viewAdd.getExecutorId();
                    String albumGenre = viewAdd.getAlbumGenre();

                    if (albumId != null && !albumName.equals("") && executorId != null && !albumGenre.equals("")) {
                        data.insert(albumId, albumName, executorId, albumGenre);
                        updateTable();
                    }
                }
            }
        });
        JButton update = new JButton("Редактировать");
        update.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (viewUpdate == null) viewUpdate = new ViewUpdate();
                if (viewUpdate.showDialog(View.this, "Insert")) {
                    Integer albumId = viewUpdate.getAlbumId();
                    String albumName = viewUpdate.getAlbumName();
                    Integer executorId = viewUpdate.getExecutorId();
                    String albumGenre = viewUpdate.getAlbumGenre();

                    if (albumId != null && !albumName.equals("") && executorId != null && !albumGenre.equals("")) {
                        data.update(albumId, albumName, executorId, albumGenre);
                        updateTable();
                    }
                }
            }
        });

        JButton remove = new JButton("Удалить");
        remove.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (viewDelete == null) viewDelete = new ViewDelete();

                if (viewDelete.showDialog(View.this, "Delete album")) {
                    Integer albumId = viewDelete.getAlbumId();

                    if (albumId != null) {
                        data.delete(albumId);
                        updateTable();
                    }

                }

            }
        });

        // Формирование интерфейса
        Box contents = new Box(BoxLayout.Y_AXIS);
        contents.add(new JScrollPane(table));
        getContentPane().add(contents);

        JPanel buttons = new JPanel();
        buttons.add(query);
        buttons.add(add);
        buttons.add(remove);
        buttons.add(update);
        getContentPane().add(buttons, "South");
        // Вывод окна на экран
        setSize(600, 300);
        setVisible(true);


    }

    private void updateTable() {
        tableModel.setColumnIdentifiers(columnsHeader);
        tableModel.getDataVector().removeAllElements();
        records = data.selectAll();
        array = new Object[records.size()][4];
        for (int i = 0; i < records.size(); i++) {
            array[i][0] = records.get(i).getAlbumId();
            array[i][1] = records.get(i).getAlbumName();
            array[i][2] = records.get(i).getExecutorId();
            array[i][3] = records.get(i).getAlbumGenre();
        }
        // Наполнение модели данными
        for (int i = 0; i < array.length; i++) {

            tableModel.addRow(array[i]);///?????
        }
    }
}

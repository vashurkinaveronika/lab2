public class TableAlbum {
    private int albumId;
    private String albumName;
    private int executorId;
    private String albumGenre;

    public TableAlbum(int albumId, String albumName, int executorId, String albumGenre) {
        this.albumId = albumId;
        this.albumName = albumName;
        this.executorId = executorId;
        this.albumGenre = albumGenre;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public int getExecutorId() {
        return executorId;
    }

    public void setExecutorId(int executorId) {
        this.executorId = executorId;
    }

    public String getAlbumGenre() {
        return albumGenre;
    }

    public void setAlbumGenre(String albumGenre) {
        this.albumGenre = albumGenre;
    }

    @Override
    public String toString() {
        return "TableAlbum{" +
                "albumId=" + albumId +
                ", albumName='" + albumName + '\'' +
                ", executorId=" + executorId +
                ", albumGenre='" + albumGenre + '\'' +
                '}';
    }
}

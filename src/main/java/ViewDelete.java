import javax.swing.*;
import java.awt.*;

public class ViewDelete extends JPanel {
    private JTextField albumId;

    private JButton ok;
    private boolean isOk;
    private JDialog jDialog;

    public ViewDelete() {


        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(6, 4));

        panel.add(new JLabel("Album id:"));
        panel.add(albumId = new JTextField(""));

        add(panel, BorderLayout.CENTER);


        ok = new JButton("Ok");
        ok.addActionListener(event -> {
            isOk = true;
            jDialog.setVisible(false);
        });


        JPanel buttonPanel = new JPanel();
        buttonPanel.add(ok);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    public int getAlbumId() {
        return Integer.valueOf(albumId.getText());
    }

    public boolean showDialog(Component parent, String title) {
        isOk = false;

        Frame owner = null;
        if (parent instanceof Frame)
            owner = (Frame) parent;
        else
            owner = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, parent);


        if (jDialog == null || jDialog.getOwner() != owner) {
            jDialog = new JDialog(owner, true);
            jDialog.add(this);
            jDialog.getRootPane().setDefaultButton(ok);
            jDialog.pack();
        }

        jDialog.setTitle(title);
        jDialog.setVisible(true);
        return isOk;
    }


}

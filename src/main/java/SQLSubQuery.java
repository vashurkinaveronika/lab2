

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

public class SQLSubQuery {

    private Properties properties = new Properties();
    private String url;
    private String user;
    private String password;

    public SQLSubQuery() {
        try (FileInputStream in = new FileInputStream("C:\\Users\\Вероника\\Desktop\\8 семак\\курсач WEB\\2\\Lab2\\src\\main\\resources\\application.properties")) {
            properties.load(in);
            url = properties.getProperty("jdbc.url");
            user = properties.getProperty("jdbc.user");
            password = properties.getProperty("jdbc.password");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void create() {
        try (Connection connection = DriverManager
                .getConnection(url, user, password);
             Statement statement = connection.createStatement();) {

            String sql = "CREATE TABLE album( " +
                    " album_id INT NOT NULL, " +
                    " album_name        TEXT    NOT NULL, " +
                    "executor_id INT NOT NULL," +
                    "  album_genre          TEXT  NOT NULL, " +
                    "  CONSTRAINT pk_album_id PRIMARY KEY(album_id)," +
                    " CONSTRAINT fk_executor_id FOREIGN KEY (executor_id) REFERENCES executor (executor_id));";

            statement.executeUpdate(sql);

        } catch (Exception e) {
            System.out.println(e);
        }


    }

    public ArrayList<String> doQuery(String executorName) {
        ArrayList<String> records = new ArrayList<String>();
        String sql = "SELECT album.album_name FROM album join executor on executor.executor_id=album.executor_id AND executor.executor_name='" + executorName + "';";
        try (Connection connection = DriverManager
                .getConnection(url, user, password);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql);
        ) {
            while (resultSet.next()) {
                String albumName = resultSet.getString("album_name");
                records.add(albumName);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return records;
    }


    public void insert(int albumID, String albumName, int executorId, String albumGenre) {
        try (Connection connection = DriverManager
                .getConnection(url, user, password);
             Statement statement = connection.createStatement();) {
            String sql = "INSERT INTO album "
                    + "VALUES (" + "'" + albumID + "'" + ", " + "'" + albumName + "'" + ", " + "'" + executorId + "'" + ", " + "'" + albumGenre + "'" + ");";
            statement.executeUpdate(sql);

            connection.setAutoCommit(false);
            connection.commit();

        } catch (Exception e) {
            System.out.println(e);

        }
    }

    public ArrayList<TableAlbum> selectAll() {
        ArrayList<TableAlbum> records = new ArrayList<TableAlbum>();
        try (Connection connection = DriverManager
                .getConnection(url, user, password);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM album;");) {
            while (resultSet.next()) {
                TableAlbum record = createTableAlbum(resultSet);
                records.add(record);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return records;
    }

    public ArrayList<TableAlbum> update(int albumID, String newAlbumName, int executorId, String newAlbumGenre) {
        ArrayList<TableAlbum> records = new ArrayList<TableAlbum>();
        try (Connection connection = DriverManager
                .getConnection(url, user, password);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM album;");) {

            String sql1 = "UPDATE album set album_name= " + "'" + newAlbumName + "'" + " where album_id=" + "'" + albumID + "'" + ";";

            String sql2 = "UPDATE album set album_genre = " + "'" + newAlbumGenre + "'" + " where album_id=" + "'" + albumID + "'" + ";";

            String sql3 = "UPDATE album set executor_id = " + "'" + executorId + "'" + " where album_id=" + "'" + albumID + "'" + ";";

            connection.setAutoCommit(false);
            statement.executeUpdate(sql1);
            statement.executeUpdate(sql2);
            statement.executeUpdate(sql3);
            connection.commit();
            while (resultSet.next()) {
                TableAlbum record = createTableAlbum(resultSet);
                records.add(record);
            }

        } catch (Exception e) {
            String error = new String(e.getClass().getName() + ": " + e.getMessage());
            System.out.println(error);
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return records;
    }

    public ArrayList<TableAlbum> delete(int albumID) {
        ArrayList<TableAlbum> records = new ArrayList<TableAlbum>();
        String sql = "DELETE from album where album_id= " + "'" + albumID + "'" + ";";

        try (Connection connection = DriverManager
                .getConnection(url, user, password);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM album;");
        ) {
            statement.executeUpdate(sql);
            connection.setAutoCommit(false);
            connection.commit();

            while (resultSet.next()) {
                TableAlbum record = createTableAlbum(resultSet);
                records.add(record);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return records;
    }

    private TableAlbum createTableAlbum(ResultSet resultSet) throws SQLException {
        int newAlbumID = resultSet.getInt("album_id");
        String newAlbumName = resultSet.getString("album_name");
        int newExecutorId = resultSet.getInt("executor_id");
        String newAlbumGenre = resultSet.getString("album_genre");

        return new TableAlbum(newAlbumID, newAlbumName, newExecutorId, newAlbumGenre);
    }


}
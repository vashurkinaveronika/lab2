import javax.swing.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {

        //Задание 1

        SQLQuery sqlQuery = new SQLQuery();
        System.out.println("Сформировать запрос, выводящий названия альбомов исполнителя Bond. " + sqlQuery.doQuery(sqlQuery.getQuery()));
        System.out.println("Данные после добавления: " + sqlQuery.doUpdate(sqlQuery.getAdd()));
        System.out.println("Данные после изменения: " + sqlQuery.doUpdate(sqlQuery.getUpdate()));
        System.out.println("Данные после удаления: " + sqlQuery.doUpdate(sqlQuery.getDelete()) + "\n");


        //Задание 2

        SQLSubQuery sqlSubQuery = new SQLSubQuery();
        Scanner scanner = new Scanner(System.in);
        //запрос
        System.out.println("Сформировать запрос, выводящий названия альбомов введенного исполнителя." + "\n" + "Введите имя исполнителя: ");
        System.out.println(sqlSubQuery.doQuery(scanner.next()));
        //добавление
        System.out.println("Данные до добавления: " + sqlSubQuery.selectAll());
        System.out.println("Введите данные для добавления" + "\n" + "Введите album id: ");
        int aibumId = scanner.nextInt();
        System.out.println("Введите album name: ");
        String aibumName = scanner.next();
        System.out.println("Введите executor id: ");
        int executorId = scanner.nextInt();
        System.out.println("Введите album genre: ");
        String aibumGenre = scanner.next();
        sqlSubQuery.insert(aibumId, aibumName, executorId, aibumGenre);
        //редактирование
        System.out.println("Данные после добавления: " + sqlSubQuery.selectAll());
        System.out.println("Введите данные для редактирования" + "\n" + "Введите album id: ");
        aibumId = scanner.nextInt();
        System.out.println("Введите album name: ");
        aibumName = scanner.next();
        System.out.println("Введите executor id: ");
        executorId = scanner.nextInt();
        System.out.println("Введите album genre: ");
        aibumGenre = scanner.next();
        sqlSubQuery.update(aibumId, aibumName, executorId, aibumGenre);
        //удаление
        System.out.println("Данные после изменения: " + sqlSubQuery.selectAll());
        System.out.println("Введите данные для удаления" + "\n" + "Введите album id: ");
        aibumId = scanner.nextInt();
        sqlSubQuery.delete(aibumId);
        System.out.println("Данные после удаления: " + sqlSubQuery.selectAll());


        //Задание 3
        new METAData().getMETA();

        //Задание 4

        View app = new View();
        app.setVisible(true);


    }
}

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

public class SQLQuery {

    private Properties properties = new Properties();
    private String url;
    private String user;
    private String password;

    public SQLQuery() {
        try (FileInputStream in = new FileInputStream("C:\\Users\\Вероника\\Desktop\\8 семак\\курсач WEB\\2\\Lab2\\src\\main\\resources\\application.properties")) {
            properties.load(in);
            url = properties.getProperty("jdbc.url");
            user = properties.getProperty("jdbc.user");
            password = properties.getProperty("jdbc.password");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getQuery() {
        return "SELECT album.album_name FROM album join executor on executor.executor_id=album.executor_id AND executor.executor_name='Bond';";
    }

    public String getAdd() {
        return "INSERT INTO album values (006,'nikosina',05,'Jazz');";
    }

    public String getDelete() {
        return "DELETE from album where album_id= '006';";
    }

    public String getUpdate() {
        return "UPDATE album set album_name= 'apple' where album_id='006';";
    }


    public String doQuery(String sql) {
        ArrayList albumNames = new ArrayList();
        try (Connection connection = DriverManager
                .getConnection(url, user, password);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql);) {
            while (resultSet.next()) {
                String albumName = resultSet.getString("album_name");
                albumNames.add(albumName);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return albumNames.toString();

    }

    public String doUpdate(String sql) {
        ArrayList<TableAlbum> records = new ArrayList<TableAlbum>();
        try (Connection connection = DriverManager
                .getConnection(url, user, password);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM album;");) {
            statement.executeUpdate(sql);

            while (resultSet.next()) {
                int albumID = resultSet.getInt("album_id");
                String albumName = resultSet.getString("album_name");
                int executorId = resultSet.getInt("executor_id");
                String albumGenre = resultSet.getString("album_genre");

                TableAlbum record = new TableAlbum(albumID, albumName, executorId, albumGenre);
                records.add(record);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return records.toString();
    }
}

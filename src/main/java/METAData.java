import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class METAData {

    public void getMETA() {
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/Music store",
                            "postgres", "Veronika");

            String[] types = {"TABLE"};
            ResultSet tables = c.getMetaData().getTables(null, null, "%", types);
            System.out.println("Названия таблиц БД: ");
            while (tables.next()) {
                System.out.println(tables.getString("TABLE_NAME"));
            }
            System.out.println("\n");
            System.out.println("Названия всех столбцов одной из таблиц(album): ");
            ResultSet columns = c.getMetaData().getColumns(null, null, "album", "%");
            while (columns.next()) {
                System.out.println(columns.getString("COLUMN_NAME"));
            }
            System.out.println("\n");
            c.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
